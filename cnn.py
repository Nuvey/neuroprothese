import tensorflow as tf
import random
import librosa.display
import cv2

from utils import *
from keras.models import Sequential
from keras.layers import *
from keras.utils import np_utils
from tensorflow.python.keras import backend as K
from tkinter import *
from interface import InterfaceTraining

ERROR = 99
NUMBER_FILE_PER_DIR = 60

# Creation de la fenetre
window = Tk()
interface = InterfaceTraining(window)
interface.start()

# fin du programme si on clique sur 'Quitter'
if (interface.isEnd):
    sys.exit(ERROR)

PERCENT_TRAIN = interface.get_percentTrain()
base_dir = interface.get_base_dir() + "/"

# Ferme la fenetre
window.destroy()

# Verifie que le dossier DataSet existe
if not os.path.exists(base_dir):
    print("Le dossier '{}' n'existe pas.".format(base_dir))
    sys.exit(ERROR)

# Supprime le dossier train & le dossier validation si ils existent
bTrain = base_dir + "train/"
bValid = base_dir + "validation/"
if os.path.exists(bTrain):
    shutil.rmtree(bTrain)
if os.path.exists(bValid):
    shutil.rmtree(bValid)

# Construit la liste des classes du dossier
classes = os.listdir(base_dir)

# Verifie que la liste des classes n'est pas vide (ie dossier Dataset non vide)
if not classes:
    print("Le dossier '{}' ne contient aucune classes.".format(base_dir[:-1]))
    sys.exit(ERROR)

# Construit la liste des chemin vers les dossier "classes" contenant les différents enregistrements
list_dir = []
if interface.classesList:
    i = 0
    while i < len(interface.classesList):
        if interface.classesList[i] in classes:
            list_dir.append(base_dir + interface.classesList[i])
        else:
            print("Le dossier '{}' ne contient aucune classes '{}'.".format(base_dir[:-1], interface.classesList[i]))
            sys.exit(ERROR)
        i += 1
else:
    for i in classes:
        list_dir.append(base_dir + i)
print(list_dir)

# Initialisation de tensorflow pour le GPU
if K.backend() == "tensorflow":
    import tensorflow as tf
    from tensorflow.python.client import device_lib
    print(device_lib.list_local_devices())
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.allow_growth = True
    # # session = tf.compat.v1.InteractiveSession(config=config)
    # config.gpu_options.per_process_gpu_memory_fraction = 0.2
    tf.compat.v1.keras.backend.set_session(tf.compat.v1.Session(config=config))
    # print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

# Initialise la seed pour limiter l'aleatoire entre 2 executions
from numpy.random import seed

seed(123)
tf.compat.v1.random.set_random_seed(123)

# Creation des dossier train et validation & creation des listes contenant les mel-spectrogrammes d'entrainement
# et de validation
split_data(base_dir, list_dir, PERCENT_TRAIN, NUMBER_FILE_PER_DIR)
idx_to_classes = get_idx_to_classes(list_dir)
print(idx_to_classes)
train_dir = os.path.join(base_dir, 'train')
x_train, y_train = load_dataset(train_dir, idx_to_classes)
validation_dir = os.path.join(base_dir, 'validation')
x_test, y_test = load_dataset(validation_dir, idx_to_classes)

# Affiche le nombre de donnees d'entrainement et de test
print('\nTaille du corpus total')
print('\t• train :', len(x_train), 'exemples')
print('\t• test :', len(x_test), 'exemples')

# Affiche le nombre d'images et de labels pour le train et le validation
print('\nTaille des données d\'apprentissage')
print('\t• X_train (images) :', len(x_train))
print('\t• y_train (labels) :', len(y_train))

print('\nTaille des données de test')
print('\t• X_test (images) :', len(x_test))
print('\t• y_test (labels) :', len(y_test))

# Affiche la corespondance entre une image et un label
print("\nAffichage de la correspondance des labels :")
for i in range(10):
    n = random.randint(0, len(y_train) - 1)
    print('• y_train[' + str(n) + '] =', y_train[n], '->', idx_to_classes[y_train[n]])

img_width = 128
img_height = 128

# Enregistre le nom des classes dans un fichier
with open("classes.txt", "w") as file:
    for i in idx_to_classes:
        file.write(i + "\n")

# Redimensionne l'image
for i in range(len(x_train)):
    x_train[i] = cv2.resize(x_train[i], dsize=(img_height, img_width))
for i in range(len(x_test)):
    x_test[i] = cv2.resize(x_test[i], dsize=(img_height, img_width))

# Convertit train et test en array
x_train = np.array(x_train)
y_train = np.array(y_train)
x_test = np.array(x_test)
y_test = np.array(y_test)

# Affiche la shape de train et test
print('\nTaille des données d\'apprentissage apres conversion')
print('\t• X_train (images) :', x_train.shape)
print('\t• y_train (labels) :', y_train.shape)

print('\nTaille des données de test apres conversion')
print('\t• X_test (images) :', x_test.shape)
print('\t• y_test (labels) :', y_test.shape)

# Affiche 5 images de chaque classe (spectrogrammes) et leur label associe
plt.figure(figsize=(20, 20))
n = 0
for i in range(len(idx_to_classes)):
    for j in range(5):
        # Associe image-label
        img, target = x_train[n + j], y_train[n + j]
        # Affiche le spectrogramme
        plt.subplot(len(idx_to_classes), 5, i * 5 + j + 1)
        img = librosa.power_to_db(img)
        librosa.display.specshow(img, cmap='magma')
        # Ajoute un titre a l'image
        plt.title('{} ({})'.format(idx_to_classes[target], target))
        # plt.colorbar(format='%+2.0f dB')
    n += round(x_train.shape[0] / len(idx_to_classes))
plt.show(block=False)

# Encodage One-hot
nb_classes = len(list_dir)
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)
print("\nAffichage de l'encodage des labels :")
for i in range(10):
    n = random.randint(0, len(y_train) - 1)
    print('• y_train[' + str(n) + '] =', y_train[n], '->', Y_train[n])

# Reshape de train et test
X_train = x_train.reshape(x_train.shape[0], img_width, img_height, 1)
X_test = x_test.reshape(x_test.shape[0], img_width, img_height, 1)

# Model definition
model = Sequential()
model.add(Conv2D(32, (3, 3), padding='same',
                 input_shape=X_train.shape[1:]))
model.add(Activation('relu'))
model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(nb_classes))
model.add(Activation('softmax'))

# Initialise le RMSprp optimiseur
opt = tf.keras.optimizers.RMSprop(learning_rate=0.0001, decay=1e-6)

# Compile le modèle en utilisant RMSprop
model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

# Entrainement du modele
epoch = 45
history = model.fit(X_train, Y_train, validation_data=(X_test, Y_test), epochs=epoch, batch_size=32)
                    #,callbacks=[myCallBack])#, tensorboard_callback])

plt.figure(figsize=(11, 3))

# Affiche la fonction de perte (loss)
plt.subplot(1, 2, 1)
plt.plot(history.epoch, history.history['loss'])
plt.title('loss')

# Precision de la reconnaissance
plt.subplot(1, 2, 2)
plt.plot(history.epoch, history.history['accuracy'])
plt.title('accuracy');

# Evalue le modele
scores = model.evaluate(X_test, Y_test, verbose=2)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))

# Enregitrement du modele
model_json = model.to_json()
with open("model4.json", "w") as json_file:
    json_file.write(model_json)
# Enregistrement des poids
model.save_weights("model4.h5")
print("Saved model to disk")

# Matrice de confusion
plt.figure()
predictions = model.predict(X_test)
cm = base_confusion_matrix(predictions, y_test, Y_train)
colored_confusion_matrix(cm, idx_to_classes)

plt.show()
