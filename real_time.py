# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 13:20:20 2019

@author: hugob

Ce fichier permet de réaliser un enregistrement audio ainsi que son traitement
de façon simultanée. Ceci est réalisé à l'aide de deux thread :
    - recorder : qui a comme fonction d'enregistrer l'audio
    - process : qui a comme fonction d'exécuter l'algo DTW sur le buffer du 
                recorder
"""

## Lib
import itertools
import pyaudio
import time
import numpy as np
from threading import Thread
import wave

from features import features_folder, remove_silence
from utilsDTW import generate_threshold, score_test_file, recognition_file

## Mot à reconnaitre et paramètres
keyword = "Voiture"

folder_keyword = r"./" + keyword
keyword_file = wave.open(r"./" + keyword + "/" + keyword + "1.wav",'rb')

len_wind = 0.032 # 32ms par fenêtre temporelle MFCC

n_mfcc = 30
delta = True
energy = False

global_features_keyword = features_folder(folder_keyword, n_mfcc, energy, delta)
global_features_keyword = remove_silence(global_features_keyword)

size_keyword = len_wind * max([np.shape(x)[1] for x in global_features_keyword])

## Calcul du seuil 

threshold = generate_threshold(global_features_keyword)

## Constante d'acquisition audio

FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = keyword_file.getframerate()
CHUNK = int(1.1 * size_keyword * keyword_file.getframerate()) 
RECORD_SECONDS = 5 # 10s au total
audio = pyaudio.PyAudio()

## thread recorder
class recorder(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.frames = []
        self.nb_buffer = 0

    def run(self):
        audio = pyaudio.PyAudio()
        
        """Code à exécuter pendant l'exécution du thread."""
        # start Recording
        stream = audio.open(format=FORMAT, channels=CHANNELS,
                rate=RATE, input=True,
                frames_per_buffer=CHUNK)
        
        n_chunk = 0
        buffer = []

        for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
            print("record")
            
            ## Process de bufferisation ##
            
            data = stream.read(CHUNK)     
            self.frames.append(data)
            buffer.append([data])
            
            if n_chunk != 0:
                self.nb_buffer += 1
                waveFile = wave.open('buffer' + str(self.nb_buffer) + '.wav', 'wb')
                waveFile.setnchannels(CHANNELS)
                waveFile.setsampwidth(audio.get_sample_size(FORMAT))
                waveFile.setframerate(RATE)
                waveFile.writeframes(b''.join(list(itertools.chain.from_iterable(buffer))))
                waveFile.close()
                
            if n_chunk > 1:
                buffer.pop(0)
            
            n_chunk += 1
    
        print("finished recording")
        # stop Recording
        stream.stop_stream()
        stream.close()
        audio.terminate()
        
## thread process
class process(Thread):
    def __init__(self,recorder1):
        Thread.__init__(self)
        self.m_recorder = recorder1
        self.nb_process = 1

    def run(self):
        start_time = time.time()
        last_reco = 0
        while time.time() - start_time < RECORD_SECONDS +1:
            if self.nb_process == self.m_recorder.nb_buffer:
                print("process")
                
                ## Process de reconnaissance ##
                score_buffer = score_test_file("buffer"+ str(self.m_recorder.nb_buffer) +".wav",
                                               global_features_keyword, n_mfcc, energy, delta)
                print("score obtenu :" + str(score_buffer[0]))
                reco_file = recognition_file(score_buffer, threshold)
                
                if last_reco == 1 and reco_file[0] == 1:
                    print("Résultat reconnaissance : Déjà activé")
                else:
                    print("Résultat reconnaissance :" + str(reco_file[0]))
                ## Fin de process ##
                
                last_reco = reco_file[0]
                self.nb_process += 1
    
## Création des threads
recorder1 = recorder()
process1 = process(recorder1)

## Lancement des threads
recorder1.start()
process1.start()

## Attente de finition 
recorder1.join()
process1.join()

## Fichier audio complet 
waveFile = wave.open('test.wav', 'wb')
waveFile.setnchannels(CHANNELS)
waveFile.setsampwidth(audio.get_sample_size(FORMAT))
waveFile.setframerate(RATE)
waveFile.writeframes(b''.join(recorder1.frames))
waveFile.close()

## Annexe 

# Temps de save en .wav     : ~ 10 ms (max)
# Temps de traintement      : ~ 20 ms (max)
# Temps de load par librosa : ~ 10 ms (max)