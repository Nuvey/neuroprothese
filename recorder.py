import pyaudio
import wave
import time
import os
import struct
import math

TRESHOLD = 10

SHORT_NORMALISE = (1.0 / 32768.0)
CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
TIME_OUT_RECORDING = 0.85
WAV_OUTPUT_FILENAME = "./Record"


class Recorder:

    def rms(self, frame):
        count = len(frame) / 2
        format = "%dh" % (count)
        shorts = struct.unpack(format, frame)

        sum_square = 0.0
        for sample in shorts:
            n = sample * SHORT_NORMALISE
            sum_square += n * n
        rms = math.sqrt(sum_square / count)
        return rms * 1000

    def __init__(self):
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(format=FORMAT,
                                  channels=CHANNELS,
                                  rate=RATE,
                                  input=True,
                                  output=True,
                                  frames_per_buffer=CHUNK)

    def write(self, recording):
        n_file = len(os.listdir(WAV_OUTPUT_FILENAME))
        filename = os.path.join(WAV_OUTPUT_FILENAME, 'Record{}.wav'.format(n_file))

        wf = wave.open(filename, 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(self.p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(recording)
        wf.close()
        print('Written to file : {}'.format(filename))

    def record(self):
        print("Noise detected, Recording...")
        rec = []
        current_time = time.time()
        max_time = time.time() + TIME_OUT_RECORDING

        while current_time <= max_time:
            data = self.stream.read(CHUNK)
            if self.rms(data) >= TRESHOLD:
                max_time = time.time() + TIME_OUT_RECORDING
            current_time = time.time()
            rec.append(data)
        self.write(b''.join(rec))
        print("Returning to listening")

    def listen(self):
        print("Listening beginning")
        while True:
            input = self.stream.read(CHUNK)
            rms_value = self.rms(input)
            if rms_value > TRESHOLD:
                self.record()

# input("Press any key to continue...")
# os.system('play -nq -t alsa synth {} sine {}'.format(0.08, 440))
# os.system('play -nq -t alsa synth {} sine {}'.format(0.08, 500))
# os.system('play -nq -t alsa synth {} sine {}'.format(0.08, 550))
# time.sleep(0.5)

