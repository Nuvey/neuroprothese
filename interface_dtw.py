# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 11:01:03 2020

@author: hugob
"""

from tkinter import *
from tkinter.messagebox import showwarning
import tkinter.font as tkFont

# Interface pour l'enregitrement de fichiers audio (herite de la classe Frame)
class Interface(Frame):
    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=768, height=576, **kwargs)
        self.master.title("Création du données audio d'apprentissage")
        self.master.resizable(width=False, height=False)
        self.pack(fill=BOTH)

        self.keyword = ''
        self.isEnd = False
        self.fontStyle = tkFont.Font(size=16, weight='bold')

        # Widget list
        self.textClasses = Label(self, text="Mot-clef : ", font=self.fontStyle)
        self.textClasses.pack()

        self.keyword = Entry(self, font=self.fontStyle)
        self.keyword.pack()
        
        self.message = Label(self, text="Nombre d'occurence :", font=self.fontStyle)
        self.message.pack()

        self.numberData = Spinbox(self, from_=0, to=50, font=self.fontStyle)
        self.numberData.pack()

        self.next = Button(self, text="Valider", command=self.exit, font=self.fontStyle)
        self.next.pack(side=LEFT, padx=5)

        self.end = Button(self, text="Quitter", command=self.quitt, font=self.fontStyle)
        self.end.pack(side=RIGHT, padx=5)

    # Recupere numberData
    def get_numberData(self):
        return self.numberData.get()

    # Recupere classes
    def get_classe(self):
        return self.keyword.get()

    # Ferme la fenetre si tous les champs sont valides. Message d'erreur sinon
    def exit(self):
        if self.get_classe() != '':
            self.keyword= self.get_classe()
        if not self.keyword or not int(self.get_numberData()) or int(self.get_numberData()) <= 0:
            showwarning('ERROR',
                        "Le nombre de données par mots clés doit être > 0 !!!\nSaisissez au moins un mot clé valide !!!")
            self.classes.delete(0, END)
            self.classesList = []
        else:
            self.master.quit()

    # Quitte la fenetre
    def quitt(self):
        self.isEnd = True
        self.master.quit()

    # Demarre la fenetre
    def start(self):
        self.mainloop()

