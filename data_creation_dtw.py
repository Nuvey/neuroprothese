# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 10:59:35 2020

Projet : Commande vocale pour neuroprothèse
"""

import sys
import os
import wave
import pyaudio
import time
from tkinter import *
from interface_dtw import Interface

# Global variables
CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = 2

# Redefinition de la classe Recorder
class RecorderDataSet():
    def __init__(self):
        self.audio = pyaudio.PyAudio()
        self.stream = self.audio.open(format=FORMAT,
                                  channels=CHANNELS,
                                  rate=RATE,
                                  input=True,
                                  output=True,
                                  frames_per_buffer=CHUNK)
        
    def record(self, keyword, numberData):
        for i in range(numberData):
            rec = []
            current_time = time.time()
            max_time = time.time() + RECORD_SECONDS
            print("Recording " + str(i +1) + " / " + str(numberData))
            while current_time <= max_time:
                data = self.stream.read(CHUNK)
                current_time = time.time()
                rec.append(data)
            waveFile = wave.open('./' + keyword + '/' + keyword + str(i +1) + '.wav', 'wb')
            waveFile.setnchannels(1)
            waveFile.setsampwidth(self.audio.get_sample_size(pyaudio.paInt16))
            waveFile.setframerate(RATE)
            waveFile.writeframes(b''.join(rec))
            waveFile.close()
            print("End recording " + str(i +1) + " / " + str(numberData))
            
            time.sleep(2)

# Creation de la fenetre
window = Tk()
interface = Interface(window)
interface.start()

keyword = interface.keyword
numberData = int(interface.get_numberData())

# fin du programme si on clique sur 'Quitter'
if (interface.isEnd):
    sys.exit(ERROR)

# Ferme la fenetre
window.destroy()

# Creation des dossiers Dataset
dataSetPath = "./" + interface.keyword
os.makedirs(dataSetPath, exist_ok=True)

# Creation d'un Recorder
rec = RecorderDataSet()
rec.record(keyword, numberData)