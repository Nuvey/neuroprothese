import os
import shutil
import numpy as np
import librosa
import librosa.display
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

"""
Cree les dossier train et validation
baseDir : Chemin du dossier principal ou seront cree les dossiers train et validation
dirList : Liste des dossiers contenant les fichiers audios
percentTrain : Pourcentage de fichier d'entrainement
numberFilePerDir : Nombre de donnees par dossiers
"""


def split_data(basedir, dirList, percentTrain, numberFilePerDir):
    bTrain = basedir + "train/"
    bValid = basedir + "validation/"
    os.makedirs(bTrain, exist_ok=True)
    os.makedirs(bValid, exist_ok=True)
    print("\nCreating train & validation directory...")
    for dir in dirList:
        if numberFilePerDir > len(os.listdir(dir)):
            print("ERROR\nNumber of file per directory too high ")
            exit(1)
        if os.path.isdir(dir):
            i = 0
            numberTrain = round(numberFilePerDir * (percentTrain / 100))
            bValidDir = bValid + os.path.basename(dir)
            bTrainDir = bTrain + os.path.basename(dir)
            os.makedirs(bValidDir, exist_ok=True)
            os.makedirs(bTrainDir, exist_ok=True)
            for file in os.listdir(dir):
                if i < numberFilePerDir:
                    path = os.path.join(dir, file)
                    if os.path.isfile(path):
                        if i < numberTrain:
                            shutil.copy(path, bTrainDir)
                        else:
                            shutil.copy(path, bValidDir)
                        i = i + 1


"""
Construit la liste des chemins de chaque classe
dirList : Liste des noms de classes (dossier) dont on veut le chemin
"""


def get_idx_to_classes(dirList):
    idx_to_classes = []
    for dir in dirList:
        if os.path.isdir(dir):
            idx_to_classes.append(os.path.basename(dir))
    return idx_to_classes
"""
Charge les images extraites des fichiers audios et leurs labels associés
path : Chemin du dossier
idx_to_classes : Liste des nom des classes (dossier)
"""

def load_dataset(path, idx_to_classes):
    spec = []
    labels = []

    for idx, label in enumerate(idx_to_classes):
        with os.scandir(os.path.join(path, label)) as it:
            for entry in it:
                if (not entry.name.startswith('.')
                        and entry.name.endswith('.wav')):
                    # load audio
                    y, sr = librosa.load(entry.path)
                    # convert audio to melspectrogram
                    spec.append(librosa.feature.melspectrogram(y, sr,
                                                               n_fft=2048,
                                                               hop_length=512,
                                                               power=2.0))
                    # add associated label
                    labels.append(idx)
    # mélange les données
    # combined = list(zip(spec, labels))
    # random.shuffle(combined)
    # spec, labels = zip(*combined)
    return spec, labels


"""
Cree et affiche une matrice de confusion simple
prediction : Prediction pour les echantillons d'entree
y_test : Contient les labels de test
Y_train : Contient les labels d'entrainement (utilise pour recuperer le numero des labels)
"""


def base_confusion_matrix(predictions, y_test, Y_train):
    print('Confusion matrix (rows: true classes; columns: predicted classes):');
    print()
    cm = confusion_matrix(y_test, np.argmax(predictions, axis=1), labels=list(range(Y_train.shape[1])))
    print(cm);
    print()

    print('Classification accuracy for each class:');
    print()
    for i, j in enumerate(cm.diagonal() / cm.
            sum(axis=1)):
        print("%d: %.4f" % (i, j))
    return cm


"""
Affiche une matrice de confusion avec couleurs
cm : Une matrice de confusion simple
idx_to_classes : Nom de chaque classe
"""


def colored_confusion_matrix(cm, idx_to_classes):
    # afficage de la matrice de confusion dans un autre format
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.hot_r)
    plt.title('Confusion matrix')
    plt.colorbar()
    tick_marks = np.arange(len(idx_to_classes))
    plt.grid(None)
    classes_name = zip(idx_to_classes, list(range(len(idx_to_classes))))
    classes_name = ['{} ({})'.format(*i) for i in classes_name]
    plt.xticks(tick_marks, classes_name, rotation=45)
    plt.yticks(tick_marks, classes_name)

    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            plt.text(j, i, cm[i, j], horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()
