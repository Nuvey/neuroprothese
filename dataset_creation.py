import sys
import os
import wave
import pyaudio
import time
from recorder import Recorder
from tkinter import *
from interface import Interface, InterfaceListening
# Global variables
ERROR = 99
CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
TRESHOLD = 8
TIME_OUT_RECORDING = 0.85

# Creation de la fenetre
window = Tk()
interface = Interface(window)
interface.start()

classesList = interface.classesList
numberData = int(interface.get_numberData())

# fin du programme si on clique sur 'Quitter'
if (interface.isEnd):
    sys.exit(ERROR)

# Ferme la fenetre
window.destroy()

# Creation du dossier Dataset
dataSetPath = "./Dataset/"
os.makedirs(dataSetPath, exist_ok=True)


# Redefinition de la classe Recorder
class RecorderDataSet(Recorder):

    def write(self, recording, path):
        n_file = len(os.listdir(path))
        name = path.split("/")[-2]
        filename = os.path.join(path, name + '_{}.wav'.format(n_file))

        wf = wave.open(filename, 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(self.p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(recording)
        wf.close()
        print('Written to file : {}'.format(filename))
        print("Returning to listening\n")

    def record(self, path):
        print("Noise detected, Recording...")
        rec = []
        current_time = time.time()
        max_time = time.time() + TIME_OUT_RECORDING

        while current_time <= max_time:
            data = self.stream.read(CHUNK)
            if self.rms(data) >= TRESHOLD:
                max_time = time.time() + TIME_OUT_RECORDING
            current_time = time.time()
            rec.append(data)
        self.write(b''.join(rec), path)

    def listen(self, path):
        print("Listening beginning")
        numberRecord = 0
        while True:
            print("\r" + path.split("/")[-2] + " " + str(numberRecord) + "/" + str(numberData), end='')
            input = self.stream.read(CHUNK)
            rms_value = self.rms(input)
            if rms_value > TRESHOLD:
                print("\n")
                self.record(path)
                numberRecord += 1
            if numberRecord >= numberData:
                print("\r" + path.split("/")[-2] + " " + str(numberRecord) + "/" + str(numberData), end='\n')
                break

# Creation d'un Recorder
rec = RecorderDataSet()

# Fenetre notifiant le debut de l'enregistrement
windowListenning = Tk()
interfaceListening = InterfaceListening(windowListenning)
interfaceListening.start()
# for i in range(5):
#     wave.
#     os.system('play -nq -t alsa synth {} sine {}'.format(0.08, 1500))
#     os.system('play -nq -t alsa synth {} sine {}'.format(0.08, 1200))
#     os.system('play -nq -t alsa synth {} sine {}'.format(0.08, 1000))
for i in interface.classesList:
    classIPath = dataSetPath + i + "/"
    os.makedirs(classIPath, exist_ok=True)
    rec.listen(classIPath)
