# -*- coding: utf-8 -*-
"""
Projet : Commande vocale pour neuroprothèse

Cette librairie rassemble des fonctions se divisant en deux catégories d'utilité :
    - Les fonctions nécessaires à la réalisation de la décision de reconnaissance. 
    - Des fonctions utiles à l'étude de performances, d'influence de paramètres ou encore à l'obervations de résultats 
    cachés lors de la reconnaissance. L'utilisation de ces dernières peut donc être interessante pour de potentielles 
    futures améliorations du produit livré à la fin du projet.
    
Concernant les fonctions essentielles, plusieurs étapes se distinguent :
 - Une première phase de calcul d'un seuil à partir de données d'entrainement est réalisée. Celle-ci peut être 
 faite hors-ligne (comme implémentée dans 'realtime_v2')
 - Une deuxième partie calcule le score de similarité obtenu par un fichier audio à tester vis-à-vis du mot clef. 
 Cette étape est réalisée en exécutant l'algorithme DTW sur les paramètres du fichier audio à tester et les paramètres 
 de l'ensemble des fichiers audio correspondant au mot clef
 - Enfin, la dernière étape compare le score de similarité alors obtenu et le compare avec le seuil calculé 
 précédemment.
"""
import os
from features import features_folder, features_file, remove_silence
from dtw import dtw
import numpy as np
import wave

##################################################
### Fonctions essentielles à la reconnaissance ###
##################################################
        
def generate_threshold(global_feat_train, coeff = 1.3):
    ## Function description ## 
    """
    Cette fonction renvoie un scalaire correspondant au seuil de décision de reconnaissance. Celui-ci est calculé 
    avant chaque lancement de reconnaissance, mais ce calcul peut être réalisé hors-ligne uniquement une fois avec 
    de légères modifications de façon à optimiser le temps de lancement.
    
    Ce seuil correspond au score maximal (borne supérieure) obtenu lors de l'exécution de l'algorithme DTW sur les 
    paramètres des fichiers audio d'entrainement [global_feat_train] (1er param), pris deux-à-deux, à un coefficient 
    d'erreur près [coeff], par défaut pris à 1.3 (correspondant à une marge d'erreur de 30% en situation réelle). 
    Concrètement, il s'agit de la "distance" maximale obtenue pour des mots identiques (les mots d'entrainement), 
    à laquelle est ajoutée une erreur prise par défaut à 30% ici. Le principe de reconnaissance est donc que si un 
    enregistrement à tester obtient un score (calculé par la fonction 'score_test_file') inférieur à ce seuil, il 
    sera reconnu comme étant un mot clef (correspondant aux données d'entrainement [global_feat_train])
           
    Info pratique : Le coefficient d'erreur a été ici fixé par défaut à 1.3, autorisant une marge d'erreur de 30% 
    en situation réelle d'utilisation (bruits d'environnement, mauvaise pronociation, etc). Cette valeur a été 
    trouvé de manière empirique à travers la réalisation de tests divers. Il faut comprendre que ce paramètre est 
    d'une importance cruciale puisqu'il défini la frontière entre une détection ou non. Une valeur trop importante
    mènera vers un taux de faux positif excessif, alors qu'une valeur trop faible entrainera un taux de vrai positif
    peut être un peu faible. Les matrices de confusion de mots "proches" permettent une élaboration de ce paramètre
    d'une manière théorique et déductive (réalisées dans quant_seuil.py).      
    """
    ## End description ##
    
    score = []
    for i,feat1 in enumerate(global_feat_train):
        for j,feat2 in enumerate(global_feat_train):
            if i != j:
                dist,_,_,_ = dtw(feat1.T, feat2.T,
                                     dist=lambda x, y: np.linalg.norm(x - y, ord=1))
                score.append(dist)
    return coeff * np.max(score)

def compute_param(keyword, n_subdivision):
    ## Function description ## 
    """
    Cette fonction effectue le process d'initialisation de la reconnaissance vocale. Si une optimisation de temps
    de lancement de la reconnaissance est nécessaire, cette étape peut être facilement réalisée de manière hors-
    ligne. 
    
    Afin de ne pas recalculer les paramètres des données d'entrainement correspondant au mot clef [keyword] (1er param)
    à chaque traitement de buffer, cette fonction réalise l'extraction. Il s'agit de la sortie 'global_features_keyword'. 
    A partir de ces paramètres, le seuil ('threshold') est calculé grâce à la fonction 'generate_threshold'.
    La fréquence d'échantillonnage 'RATE' de l'enregistrement audio est la même que celui des données d'entrainement.
    Enfin, le buffer d'écoute de Pyaudio est pris d'une taille 'CHUNK', celle-ci dépend de la durée de prononciation
    maximale du mot clef dans les enregistrements d'entrainement, ainsi que de [n_subdivision] (2ème param)
    """
    ## End description ##
    
    ## Durée keyword
    
    folder_keyword = r"./" + keyword
    keyword_file = wave.open(r"./" + keyword + "/" + keyword + "1.wav",'rb')
    
    len_wind = 0.032 # 32ms par fenêtre temporelle MFCC
    
    n_mfcc = 30
    delta = True
    energy = False
    
    global_features_keyword = features_folder(folder_keyword, n_mfcc, energy, delta)
    global_features_keyword = remove_silence(global_features_keyword)
    
    size_keyword = len_wind * max([np.shape(x)[1] for x in global_features_keyword])
    
    ## Calcul du seuil 
    
    threshold = generate_threshold(global_features_keyword)
    
    ## Constante d'acquisition audio
    
    RATE = keyword_file.getframerate()
    CHUNK = int(1.1 * size_keyword / n_subdivision * RATE) # 10% de marge d'erreur
    keyword_file.close()
    
    return RATE, CHUNK, global_features_keyword, threshold, size_keyword

def score_test_file(file_test, global_feat_train, n_mfcc, energy, delta):
    ## Function description ## 
    """
    Cette fonction effectue l'étape de calcul de score de similarité entre le fichier audio à tester [file_test] 
    (1er param) et le mot clef. Cette fonction renvoie alors un scalaire 'score' correspondant à ce score.
    
    Ce score correspond à la valeur minimale (borne minimale) obtenue en exécutant l'algorithme DTW sur les paramètres 
    du fichier à tester et sur chacuns des paramètres des données d'entrainement [global_feat_train] (2ème param). Le
    minimum des scores obtenus est utilisé de manière à autoriser plusieurs prononciations différentes, qui doivent de 
    ce fait appartenir aux données d'entrainement.
    L'extraction des paramètres du fichier à tester nécessite les paramètres utilisés lors de la reconnaissance :
        - [n_mfcc] (3ème param)
        - [energy] (4ème param)
        - [delta]  (5ème param)
    """
    
    global_feat_test = features_file(file_test, n_mfcc, energy, delta)
    global_feat_test = remove_silence(global_feat_test)
    
    if np.size(global_feat_test) == 0:
        return [float("inf")]
    
    score = []
    
    for i,feat1 in enumerate(global_feat_test):
        S = []
        for feat2 in global_feat_train:
            dist,_,_,_ = dtw(feat1.T, feat2.T,
                             dist=lambda x, y: np.linalg.norm(x - y, ord=1))
            S.append(dist)
        score.append(min(S))
    return score

def recognition_file(score_file, threshold):
    ## Function description ## 
    """
    Cette fonction effectue l'étape de décision, la sortie 'out' (booléenne) prend alors la valeur 1 si le mot clef 
    est reconnu, et 0 sinon.
    
    Le score [score_file] du fichier à tester est comparé au seuil [threshold] : s'il est plus faible, le mot clef 
    est reconnu, sinon, il ne l'est pas.
    """
    
    out = []
    for i in score_file:
        if i < threshold:
            out.append(1)
        else:
            out.append(0)
    return out

###################################################
### Fonctions utiles à l'étude des performances ###
################################################### 

def labelizeDir(path):
    labels = []
    for root, dirs, files in os.walk(path):
        for name in files:
            tmp = os.path.basename(name).replace(".wav",'')
            tmp = ''.join([x for x in tmp if not x.isdigit()])
            labels.append(tmp)
    return labels 

def score_between_train(global_feat_train):
    score = []
    for i,feat1 in enumerate(global_feat_train):
        for j,feat2 in enumerate(global_feat_train):
            if j > i:
                dist,_,_,_ = dtw(feat1.T, feat2.T,
                                     dist=lambda x, y: np.linalg.norm(x - y, ord=1))
                score.append(dist)
    return score

def score_between_train_and_test(folder_test, global_feat_train, n_mfcc, energy, delta):
    global_feat_test = features_folder(folder_test, n_mfcc, energy, delta)
    global_feat_test = remove_silence(global_feat_test)
    
    score = []
    
    for i,feat1 in enumerate(global_feat_test):
        S = []
        for feat2 in global_feat_train:
            dist,_,_,_ = dtw(feat1.T, feat2.T,
                             dist=lambda x, y: np.linalg.norm(x - y, ord=1))
            S.append(dist)
        score.append(min(S))
    return sum(score) / len(score)

def score_test_folder(folder_test, global_feat_train, n_mfcc, energy, delta):
    l = labelizeDir(folder_test)
    
    global_feat_test = features_folder(folder_test, n_mfcc, energy, delta)
    global_feat_test = remove_silence(global_feat_test)
    
    score = []
    
    for i,feat1 in enumerate(global_feat_test):
        S = []
        for feat2 in global_feat_train:
            dist,_,_,_ = dtw(feat1.T, feat2.T,
                             dist=lambda x, y: np.linalg.norm(x - y, ord=1))
            S.append(dist)
        score.append((min(S),l[i]))
    return score

def recognition_test(score_test, threshold):
    out = []
    for i in score_test:
        if i[0] < threshold:
            out.append((1,i[1]))
        else:
            out.append((0,i[1]))
    return out

######################################
### Script d'étude de performances ###
######################################
    
folder = r"./Maman"
folder_test = r"./Tests"
n_mfcc = 30
delta = True
energy = False

global_feat_train1 = features_folder(folder, n_mfcc, energy, delta)
global_feat_train = remove_silence(global_feat_train1)

score_between_tr = score_between_train(global_feat_train)
threshold = generate_threshold(global_feat_train)   

  