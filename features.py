# -*- coding: utf-8 -*-
"""
Projet : Commande vocale pour neuroprothèse

Cette librairie rassemble l'ensemble des fonctions utiles à l'extraction de paramètres de fichiers audio donnés
en entrée de l'algorithme DTW.
"""

import os
import librosa
import librosa.display
import numpy as np
from scipy import stats

def features_folder(folder_name, n_mfcc = 12, energy = False, delta_doubledelta = False):
    ## Function description ## 
    """
    Cette fonction renvoie une liste de matrice de paramètres ('data_out') des fichiers audio contenus dans le 
    dossier [folder_name] (1er param).
    Les colonnes des matrices de paramètres de chaque fichier audio correspondent à des vecteurs de paramètres 
    d'une fenêtre temporelle de 32ms (valeur par défaut de librosa.mfcc). 
    Ces vecteurs sont alors composés de :
        - [n_mfcc] (2eme param) MFCC
        - Une valeur d'énergie + Une valeur de dérivée d'énergie + Une valeur de double dérivée d'énergie si 
        [energy] = True (3eme param)
        - n_mfcc (2eme param) dérivés des MFCC + n_mfcc (2eme param) doubles dérivés des MFCC si 
        [delta_doubledelta] = True (4eme param)
        
    Info pratique : Les études ont montré que les meilleurs résultats sont globalement obtenus avec les dérivées
    et double dérivées des MFCC mais sans les données d'énergie'
    A savoir également que la première MFCC (MFCC0) est homogène à une log-énergie. Celle-ci est utilisée pour 
    supprimer les silences des fichiers audio dans la fonciton 'remove_silence'.          
    """
    ## End description ##
        
    data_out = []

    for path, dirs, files in os.walk(folder_name):
        for filename in files :
            y, sr = librosa.load(folder_name + "\\" + filename,sr = None)
            data_out.append((y,sr))

    # Compute MFCCs
            
    mfccs_out = []
    for data in data_out:
        mfccs_out.append(stats.zscore(librosa.feature.mfcc(data[0], data[1], n_mfcc = n_mfcc)))
        
    # Compute delta MFCCs
    
    if delta_doubledelta:
        delta_mfccs_out = []
        for data in mfccs_out:
            delta_mfccs_out.append(stats.zscore(librosa.feature.delta(data, order = 1)))
        
    # Compute double-delta MFCCs
    if delta_doubledelta:
        doubledelta_mfccs_out = []
        for data in delta_mfccs_out:
            doubledelta_mfccs_out.append(stats.zscore(librosa.feature.delta(data, order = 1)))
    
    # Compute energy 
    if energy:  
        energy_out = []
        for data in data_out:
            energy_out.append(librosa.feature.rms(data[0]))
        
    # Compute delta-energy
    if delta_doubledelta and energy: 
        delta_energy_out = []
        for data in energy_out:
            delta_energy_out.append(librosa.feature.delta(data, order = 1))
        
    # Compute doubledelta-energy
    if delta_doubledelta and energy:
        doubledelta_energy_out = []
        for data in delta_energy_out:
            doubledelta_energy_out.append(librosa.feature.delta(data, order = 1))
    
    # Create the global features vector
        
    global_features_out = []
    for i in range(len(data_out)):
        if delta_doubledelta and energy:
            global_features_out.append(np.concatenate((mfccs_out[i],delta_mfccs_out[i],
                                                        doubledelta_mfccs_out[i],energy_out[i],
                                                        delta_energy_out[i], doubledelta_energy_out[i])))
        elif delta_doubledelta and not energy:
            global_features_out.append(np.concatenate((mfccs_out[i],delta_mfccs_out[i],
                                                        doubledelta_mfccs_out[i])))
        elif not delta_doubledelta and energy:
            global_features_out.append(np.concatenate((mfccs_out[i],energy_out[i])))
        
        else:
            global_features_out.append(mfccs_out[i])
            
    # Output
    return global_features_out

def features_file(file_name, n_mfcc = 12, energy = False, delta_doubledelta = False):
    ## Function description ## 
    """
    Cette fonction renvoie une liste (de taille 1) contenant la matrice de paramètres ('data_out') du fichier 
    audio d'adresse [file_name] (1er param).
    Les colonnes de cette matrice de paramètres correspondent à des vecteurs de paramètres 
    d'une fenêtre temporelle de 32ms (valeur par défaut de librosa.mfcc). 
    Ces vecteurs sont alors composés de :
        - [n_mfcc] (2eme param) MFCC
        - Une valeur d'énergie + Une valeur de dérivée d'énergie + Une valeur de double dérivée d'énergie si 
        [energy] = True (3eme param)
        - n_mfcc (2eme param) dérivés des MFCC + n_mfcc (2eme param) doubles dérivés des MFCC si 
        [delta_doubledelta] = True (4eme param)
            
    Info pratique : Les études ont montré que les meilleurs résultats sont globalement obtenus avec les dérivées
    et double dérivées des MFCC mais sans les données d'énergie'
    A savoir également que la première MFCC (MFCC0) est homogène à une log-énergie. Celle-ci est utilisée pour 
    supprimer les silences des fichiers audio dans la fonciton 'remove_silence'.          
    """
    ## End description ##
    
    data_out = []
    y, sr = librosa.load(file_name,sr = None)
    data_out.append((y,sr))

    # Compute MFCCs
            
    mfccs_out = []
    for data in data_out:
        mfccs_out.append(stats.zscore(librosa.feature.mfcc(data[0], data[1], n_mfcc = n_mfcc)))
        
    # Compute delta MFCCs
    
    if delta_doubledelta:
        delta_mfccs_out = []
        for data in mfccs_out:
            delta_mfccs_out.append(stats.zscore(librosa.feature.delta(data, order = 1)))
        
    # Compute double-delta MFCCs
    if delta_doubledelta:
        doubledelta_mfccs_out = []
        for data in delta_mfccs_out:
            doubledelta_mfccs_out.append(stats.zscore(librosa.feature.delta(data, order = 1)))
    
    # Compute energy 
    if energy:  
        energy_out = []
        for data in data_out:
            energy_out.append(librosa.feature.rms(data[0]))
        
    # Compute delta-energy
    if delta_doubledelta and energy: 
        delta_energy_out = []
        for data in energy_out:
            delta_energy_out.append(librosa.feature.delta(data, order = 1))
        
    # Compute doubledelta-energy
    if delta_doubledelta and energy:
        doubledelta_energy_out = []
        for data in delta_energy_out:
            doubledelta_energy_out.append(librosa.feature.delta(data, order = 1))
    
    # Create the global features vector
        
    global_features_out = []
    for i in range(len(data_out)):
        if delta_doubledelta and energy:
            global_features_out.append(np.concatenate((mfccs_out[i],delta_mfccs_out[i],
                                                        doubledelta_mfccs_out[i],energy_out[i],
                                                        delta_energy_out[i], doubledelta_energy_out[i])))
        elif delta_doubledelta and not energy:
            global_features_out.append(np.concatenate((mfccs_out[i],delta_mfccs_out[i],
                                                        doubledelta_mfccs_out[i])))
        elif not delta_doubledelta and energy:
            global_features_out.append(np.concatenate((mfccs_out[i],energy_out[i])))
        
        else:
            global_features_out.append(mfccs_out[i])
            
    # Output
    return global_features_out

def remove_silence(global_features_with_silence, threshold = -5, remove_mfcc0 = True):
    ## Function description ## 
    """
    Cette fonction renvoie la liste de matrice de paramètres ('global_features_without_silence') correspondant à 
    la liste [global_features_with_silence] (1er param) dans laquelle à été supprimer les vecteurs extraits d'une
    fenêtre de silence. La première MFCC (MFCC0) étant homogène à une log-énergie. Celle-ci est seuillée grâce à 
    la valeur [threshold] (2eme param). Empiriquement, cette valeur a été ficée à -5. Un paramètre homogène à une
    énergie rendrait la reconnaissance subjective à la distance ou l'intensité de la parole de l'utilisateur, c'est
    pourquoi il est judicieux de supprimer la première MFCC. Ceci est réalisé si [remove_mfcc0] = True.
    
    Info pratique : Cette étape est cruciale pour toute l'opération de reconnaissance afin de ne comparer à l'aide
    de l'algorithme DTW que des données utiles.            
    """
    ## End description ##
    
    global_features_without_silence = []
    for feat in global_features_with_silence:
        global_features_without_silence.append((np.array([x for x in feat.T if x[0] > threshold]).T)[int(remove_mfcc0):])
        
    return global_features_without_silence
        