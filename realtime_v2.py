# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 13:33:59 2020

@author: hugob
"""

## Lib
import itertools
import pyaudio
import time
import numpy as np
from threading import Thread
import wave

from features import features_folder, remove_silence
from utilsDTW import generate_threshold, score_test_file, recognition_file

RECORD_SECONDS = 10 # 10s au total

def compute_param(keyword, n_subdivision):
    ## Durée keyword
    
    folder_keyword = r"./" + keyword
    keyword_file = wave.open(r"./" + keyword + "/" + keyword + "1.wav",'rb')
    
    len_wind = 0.032 # 32ms par fenêtre temporelle MFCC
    
    n_mfcc = 30
    delta = True
    energy = False
    
    global_features_keyword = features_folder(folder_keyword, n_mfcc, energy, delta)
    global_features_keyword = remove_silence(global_features_keyword)
    
    size_keyword = len_wind * max([np.shape(x)[1] for x in global_features_keyword])
    
    ## Calcul du seuil 
    
    threshold = generate_threshold(global_features_keyword)
    
    ## Constante d'acquisition audio
    
    RATE = keyword_file.getframerate()
    CHUNK = int(1.1 * size_keyword / n_subdivision * keyword_file.getframerate()) 
    keyword_file.close()
    
    return RATE, CHUNK, global_features_keyword, threshold, size_keyword

## thread recorder
class recorder(Thread):
    def __init__(self, keyword, n_subdivision):
        Thread.__init__(self)
        self.keyword = keyword
        self.n_subdivision = n_subdivision
        self.nb_buffer = 0
        self.time_buffer = []

    def run(self):
        audio = pyaudio.PyAudio()
        
        RATE, CHUNK, _, _, _ = compute_param(self.keyword, self.n_subdivision)
        
        """Code à exécuter pendant l'exécution du thread."""
        # start Recording
        stream = audio.open(format=pyaudio.paInt16, channels=1,
                rate=RATE, input=True,
                frames_per_buffer=CHUNK)
        
        n_chunk = 0
        buffer = []

        for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
            #print("record")
            self.time_buffer.append(time.time())
            
            ## Process de bufferisation ##
            
            data = stream.read(CHUNK)     
            buffer.append([data])
            
            if n_chunk > n_subdivision -1:
                waveFile = wave.open('./Buffers/buffer' + str(self.nb_buffer %15) + '.wav', 'wb')
                print(self.nb_buffer)
                waveFile.setnchannels(1)
                waveFile.setsampwidth(audio.get_sample_size(pyaudio.paInt16))
                waveFile.setframerate(RATE)
                waveFile.writeframes(b''.join(list(itertools.chain.from_iterable(buffer))))
                waveFile.close()
                
                self.nb_buffer += 1
                
            if n_chunk > n_subdivision:
                buffer.pop(0)
            
            n_chunk += 1
    
        print("finished recording")
        # stop Recording
        stream.stop_stream()
        stream.close()
        audio.terminate()
        
## thread process
class process(Thread):
    def __init__(self,recorder, keyword, n_subdivision):
        Thread.__init__(self)
        self.m_recorder = recorder
        self.keyword = keyword
        self.n_subdivision = n_subdivision
        self.nb_process = 1
        
        self.time_buffer = []

    def run(self):
        _, _, global_features_keyword, threshold, size_keyword = compute_param(self.keyword, self.n_subdivision)
        n_mfcc = 30
        delta = True
        energy = False
        
        start_time = time.time()
        
        last_reco = 0
        
        while time.time() - start_time < 11:
            if self.nb_process < self.m_recorder.nb_buffer:
                print("Erreur : Temps de traitement trop long vis-à-vis du temps de chunk")
            
            if self.nb_process == self.m_recorder.nb_buffer:
                #print("process")
                
                ## Process de reconnaissance ##
                score_buffer = score_test_file("./Buffers/buffer"+ str((self.m_recorder.nb_buffer -1) %15) +".wav",
                                               global_features_keyword, n_mfcc, energy, delta)
                print("Buffer " + str(self.m_recorder.nb_buffer) + " - Score obtenu :" + str(score_buffer[0]))
                reco_file = recognition_file(score_buffer, threshold)
                
                if last_reco == 0 and reco_file[0] == 1:
                    print("------> Etat de sortie : Activé")
                elif last_reco == 1 and reco_file[0] == 1:
                    print("------> Etat de sortie : Déjà activé")
                else:
                    print("------> Etat de sortie : Non-activé")
                ## Fin de process ##
                
                self.time_buffer.append(time.time() - size_keyword)
                
                last_reco = reco_file[0]
                self.nb_process += 1
                
                
keyword = "MamanNewMC"

n_subdivision = 1
time_reaction = []

# Création des threads
recorder1 = recorder(keyword, n_subdivision)
process1 = process(recorder1, keyword, n_subdivision)
    
# Lancement des threads
recorder1.start()
process1.start()
    
# Attente de finition 
recorder1.join()
process1.join()

# for n_subdivision in range(3, 4):
#     ## Création des threads
#     recorder1 = recorder(keyword, n_subdivision)
#     process1 = process(recorder1, keyword, n_subdivision)
    
#     ## Lancement des threads
#     recorder1.start()
#     process1.start()
    
#     ## Attente de finition 
#     recorder1.join()
#     process1.join()
    
#     ## Time reaction
    
#     time_reaction.append([a_i - b_i for a_i, b_i in zip(process1.time_buffer, recorder1.time_buffer)])

# import matplotlib.pyplot as plt


# plt.figure()
# n_subdivision = [n for n in range(1,len(time_reaction) +1)]
# time_reaction_avg = [np.mean(time_reaction[i]) for i in range(len(time_reaction))]
# plt.plot(n_subdivision, time_reaction_avg)
# plt.title('Temps de réaction en fonction du nombre de subdivision')
# plt.xlabel("Nombre de subdivision")
# plt.ylabel('Temps de réaction moyen (en s)')

## Annexe 

# Temps de save en .wav     : ~ 10 ms (max)
# Temps de traintement      : ~ 20 ms (max)
# Temps de load par librosa : ~ 10 ms (max)