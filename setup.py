from cx_Freeze import setup, Executable

executables = [Executable("dataset_creation.py")]

setup(
    name = "dataset_creation",
    version = "1.0",
    description = 'Creation de dataset',
    executables = executables
)