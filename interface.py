from tkinter import *
from tkinter.messagebox import showwarning
import tkinter.font as tkFont

# Interface pour l'enregitrement de fichiers audio (herite de la classe Frame)
class Interface(Frame):
    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=768, height=576, **kwargs)
        self.master.title("Création du DataSet")
        self.master.resizable(width=False, height=False)
        self.pack(fill=BOTH)

        self.classesList = []
        self.isEnd = False
        self.fontStyle = tkFont.Font(size=16, weight='bold')

        # Widget list
        self.message = Label(self, text="Entrez le nombre de données par mots à enregistrer : ", font=self.fontStyle)
        self.message.pack()

        self.numberData = Spinbox(self, from_=0, to=100000, font=self.fontStyle)
        self.numberData.pack()

        self.textClasses = Label(self, text="Saisissez un mot : ", font=self.fontStyle)
        self.textClasses.pack()

        self.classes = Entry(self, font=self.fontStyle)
        self.classes.pack()

        self.next = Button(self, text="Valider", command=self.exit, font=self.fontStyle)
        self.next.pack(side=LEFT, padx=5)

        self.finish = Button(self, text="Saisir un autre mot", command=self.refresh, font=self.fontStyle)
        self.finish.pack(side=LEFT, padx=55)

        self.end = Button(self, text="Quitter", command=self.quitt, font=self.fontStyle)
        self.end.pack(side=RIGHT, padx=5)

    # Recupere numberData
    def get_numberData(self):
        return self.numberData.get()

    # Recupere classes
    def get_classe(self):
        return self.classes.get()

    # Met a jour la fenetre si une classe a ete saisie
    def refresh(self):
        if int(self.get_numberData() == True):
            self.numberData.config(state=DISABLED)
        if self.get_classe() != '':
            self.classesList.append(self.get_classe())
        self.classes.delete(0, END)

    # Ferme la fenetre si tous les champs sont valides. Message d'erreur sinon
    def exit(self):
        if self.get_classe() != '':
            self.classesList.append(self.get_classe())
        if not self.classesList or not int(self.get_numberData()) or int(self.get_numberData()) <= 0:
            showwarning('ERROR',
                        "Le nombre de données par mots clés doit être > 0 !!!\nSaisissez au moins un mot clé valide !!!")
            self.classes.delete(0, END)
            self.classesList = []
        else:
            self.master.quit()

    # Quitte la fenetre
    def quitt(self):
        self.isEnd = True
        self.master.quit()

    # Demarre la fenetre
    def start(self):
        self.mainloop()

# Interface pour notifier le decompte avant l'ecoute (herite de la classe Frame)
class InterfaceListening(Frame):
    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=768, height=576, **kwargs)
        self.master.title("Enregistrement")
        self.master.resizable(width=False, height=False)
        self.pack(fill=BOTH)

        self.cpt = 3
        self.txt = StringVar()
        self.fontStyle = tkFont.Font(size=16, weight='bold')

        self.count = Label(self, textvariable=self.txt, font=self.fontStyle)
        self.count.pack()
        self.countdowm()

    # Demarre la fenetre
    def start(self):
        self.mainloop()

    # Met a jour la fenetre toute les secondes
    def countdowm(self):
        if self.cpt > 0:
            self.txt.set("L'enregistrement débute dans {} secondes.".format(self.cpt))
            self.cpt -= 1
            self.master.after(1000, self.countdowm)
        else:
            self.master.destroy()

# Interface pour l'entrainement du cnn (herite de la classe Frame)
class InterfaceTraining(Frame):
    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=768, height=576, **kwargs)
        self.master.title("Paramètres de l'entraînement")
        self.master.resizable(width=False, height=False)
        self.pack(fill=BOTH)

        self.classesList = []
        self.isEnd = False
        self.fontStyle = tkFont.Font(size=16, weight='bold')

        # Widget list
        self.message = Label(self, text="Saisissez le nom du dossier servant de DataSet : ", font=self.fontStyle)
        self.message.pack()

        self.base_dir = Entry(self, font=self.fontStyle)
        self.base_dir.pack()

        self.textClasses = Label(self,
                                 text="Saisissez le nom des classes à prendre pour l'entraînement (toutes les classes du dossier DataSet par défaut) : ",
                                 font=self.fontStyle)
        self.textClasses.pack()

        self.classes = Entry(self, font=self.fontStyle)
        self.classes.pack()

        self.percent = Label(self, text="Choisissez le poucentage de données pour l'entraînement : ", font=self.fontStyle)
        self.percent.pack()

        self.percentTrain = Scale(self,orient='horizontal', tickinterval=10, length=350)
        self.percentTrain.set(80)
        self.percentTrain.pack()

        self.next = Button(self, text="Valider", command=self.exit, font=self.fontStyle)
        self.next.pack(side=LEFT, padx=5)

        self.finish = Button(self, text="Saisir une autre classe", command=self.refresh, font=self.fontStyle)
        self.finish.pack(side=LEFT, padx=55)

        self.end = Button(self, text="Quitter", command=self.quitt, font=self.fontStyle)
        self.end.pack(side=RIGHT, padx=5)

    # Recupere base_dir
    def get_base_dir(self):
        return self.base_dir.get()

    # Recupere classes
    def get_classe(self):
        return self.classes.get()

    # Recupere percenTrain
    def get_percentTrain(self):
        return self.percentTrain.get()

    # Met a jour la fenetre si une classe a ete saisie
    def refresh(self):
        if self.get_base_dir() != '':
            self.base_dir.config(state=DISABLED)
        if self.get_classe() != '':
            self.classesList.append(self.get_classe())
        self.classes.delete(0, END)

    # Ferme la fenetre si tous les champs sont valides. Message d'erreur sinon
    def exit(self):
        if self.get_classe() != '':
            self.classesList.append(self.get_classe())
        if self.get_base_dir() == '':
            showwarning('ERROR',
                        "Le dossier DataSet n'est pas valide !")
            self.classes.delete(0, END)
            self.classesList = []
        elif self.get_percentTrain() == 0 or self.get_percentTrain() == 100:
            print("pourere")
            showwarning('ERROR',
                        "Le poucentage de données pour l'entraînement invalide (doit être >0 et <100) !")
        else:
            self.master.quit()

    # Demarre la fenetre
    def start(self):
        self.mainloop()

    # Quitte la fenetre
    def quitt(self):
        self.isEnd = True
        self.master.quit()
