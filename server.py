# Definition du serveur

import socket, sys, librosa, cv2, os
import numpy as np
from keras.models import model_from_json

HOST = 'localhost'
PORT = 50000  # Port de connexion (local)
counter = 0  # Compteur de connexions actives
TRESHOLD = 0.95  # Seuil de detection
img_width = 128  # Largeur de l'image
img_height = 128  # Hauteur de l'image

# Chargement du modele
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# Chargement des poids du modele
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

# Creation de la socket :
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Liaison de la socket à une adresse precise :
try:
    mySocket.bind(('', PORT))
except socket.error:
    print("La liaison du socket à l'adresse choisie a échoué.")
    sys.exit

while 1:
    # Attente de la requete de connexion d'un client :
    print("Serveur prêt, en attente de requêtes ...")
    mySocket.listen(2)

    # Etablissement de la connexion :
    connexion, adresse = mySocket.accept()
    counter += 1
    print("Client connecté, adresse IP %s, port %s" % (adresse[0], adresse[1]))

    # Dialogue avec le client :
    msgServeur = "Vous êtes connecté au serveur. Envoyez vos messages."
    connexion.send(msgServeur.encode("Utf8"))

    # Attente de message du client
    msgClient = connexion.recv(1024).decode("Utf8")
    while 1:
        print("C>", msgClient)
        # Traitement du signal audio enregistre
        if (msgClient == "Fichier ecrit"):
            # Conversion en image et prediction
            img = []
            y, sr = librosa.load("./Record/Record0.wav")
            img.append(librosa.feature.melspectrogram(y, sr, n_fft=2048, hop_length=512, power=2.0))
            img[0] = cv2.resize(img[0], dsize=(img_width, img_height))
            img = np.array(img)
            img = img.reshape(1, img_width, img_height, 1)
            prediction = loaded_model.predict(img)
            print("Prediction : ", prediction)

            # Determine la meilleure prediction et verifie si elle est satisfaisante
            for i in range(prediction.shape[0]):
                listPrediction = list(prediction[i])
                maxi = max(listPrediction)
                indMaxi = listPrediction.index(maxi)
                print("Valeur max = ", maxi, "; Indice = ", indMaxi)
                if (maxi >= TRESHOLD):
                    print("MOT RECONNU !!!")
                    with open("classes.txt", "r") as file:
                        msgServeur = "Le mot reconnu est : " + file.readlines()[indMaxi]
                else:
                    print("MOT NON RECONNU !!!!\n")
                    msgServeur = "Mot non reconnu"
            os.remove("./Record/Record0.wav")
        else:
            msgServeur = "Reconnaissance non realisee"
        connexion.send(msgServeur.encode("Utf8"))
        msgClient = connexion.recv(1024).decode("Utf8")

    # Fermeture de la connexion :
    print("Connexion interrompue.")
    connexion.close()
