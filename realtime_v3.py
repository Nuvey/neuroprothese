# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 13:20:20 2019

@author: hugob

Ce fichier permet de réaliser un enregistrement audio ainsi que son traitement
de façon simultanée. Ceci est réalisé à l'aide de deux thread :
    - recorder : qui a comme fonction d'enregistrer l'audio
    - process : qui a comme fonction d'exécuter l'algo DTW sur le buffer du 
                recorder
"""

## Lib
import itertools
import pyaudio
import time
from threading import Thread
import wave

from utilsDTW import score_test_file, recognition_file, compute_param

## Global parameters ## 

RECORD_SECONDS = 5

## thread recorder
class recorder(Thread):
    ## Class description ## 
    """
    Cette classe permet de créer un objet réalisant le processus de bufferisation, il crée les fichiers audio à tester 
    (le traitement sera réalisé par un objet 'process'). Ce processus doit s'effectuer d'une manière adaptée au mot clef
    [keyword] et au nombre de sous-division de ce mot clef [n_subdivision], c'est à dire qu'il permet la création de
    buffers respectant la contrainte suivante : une énonciation du mot clef est nécessairement entièrement inclue dans 
    au moins un des fichiers à tester 'buffer.wav'. Ceci est également une contrainte cruciale à respecter du fait de 
    l'utilisation de l'algorithme DTW. Les données sont localement mémorisés dans une file, avant d'être enregistré en 
    format WAV.
    """
    ## End description ##    
    
    def __init__(self, keywords, n_subdivision):
        Thread.__init__(self)
        self.keywords = keywords
        self.n_subdivision = n_subdivision
        self.nb_buffer = 0
        self.done_yet = False

    def run(self):
        audio = pyaudio.PyAudio()
        
        CHUNKS = []
        
        for keyword in self.keywords:
            RATE, CHUNK, _, _, _ = compute_param(keyword, self.n_subdivision, n_mfcc, delta, energy)
            CHUNKS.append(CHUNK)
            
        CHUNK = max(CHUNKS)
        
        ## Start Recording ##
        stream = audio.open(format=pyaudio.paInt16, channels=1,
                rate=RATE, input=True,
                frames_per_buffer=CHUNK)
        
        n_chunk = 0
        buffer = []

        for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):            
            ## Process de bufferisation ##
            
            data = stream.read(CHUNK)     
            buffer.append([data])
            
            if n_chunk > self.n_subdivision:
                buffer.pop(0)
            
            if n_chunk > self.n_subdivision -1:
                waveFile = wave.open('./Buffers/buffer' + str(self.nb_buffer %15) + '.wav', 'wb')
                waveFile.setnchannels(1)
                waveFile.setsampwidth(audio.get_sample_size(pyaudio.paInt16))
                waveFile.setframerate(RATE)
                waveFile.writeframes(b''.join(list(itertools.chain.from_iterable(buffer))))
                waveFile.close()
                
                print(self.nb_buffer)
                self.nb_buffer += 1
            
            n_chunk += 1
    
        print("finished recording")
        ## Stop Recording ##
        stream.stop_stream()
        stream.close()
        audio.terminate()
        
        time.sleep(2)
        self.done_yet = True
        
## thread process
class process(Thread):
    ## Class description ## 
    """
    Cette classe permet de créer un objet réalisant le processus de traitement, il utilise les fichiers audio 
    enregistrés par un objet recorder et réalise la décision de reconnaissance basée sur l'algorithme DTW et les données
    d'entrainement.
    """
    ## End description ##    
    
    def __init__(self,recorder1, keyword, n_subdivision):
        Thread.__init__(self)
        self.keywords = keywords
        self.n_subdivision = n_subdivision
        self.m_recorder = recorder1
        self.nb_process = 1

    def run(self):
        last_reco = []
        
        global_features_keywords = []
        thresholds = []
        
        for keyword in self.keywords:
            _, _, global_features_keyword, threshold, size_keyword = compute_param(keyword, 
                                                                                   self.n_subdivision, 
                                                                                   n_mfcc, delta, energy)
            global_features_keywords.append(global_features_keyword)
            thresholds.append(threshold)
            last_reco.append(0)
        
        while not(self.m_recorder.done_yet):
            if self.nb_process < self.m_recorder.nb_buffer:
                print("Erreur : Temps de traitement trop long vis-à-vis du temps de chunk")            
            
            if self.nb_process == self.m_recorder.nb_buffer:
                ## Process de reconnaissance ##
                for i, global_features_keyword in enumerate(global_features_keywords):
                    score_buffer = score_test_file("./Buffers/buffer"+ str((self.m_recorder.nb_buffer -1) %15) +".wav",
                                               global_features_keyword, n_mfcc, energy, delta)
                    print("Buffer " + str(self.m_recorder.nb_buffer -1) + " - " + keywords[i] + " - Score obtenu :" + str(score_buffer[0]))
                    reco_file = recognition_file(score_buffer, thresholds[i])
                    
                    if last_reco[i] == 0 and reco_file[0] == 1:
                        print("------> Etat de sortie : Activé")
                    elif last_reco[i] == 1 and reco_file[0] == 1:
                        print("------> Etat de sortie : Déjà activé")
                    else:
                        print("------> Etat de sortie : Non-activé")
                    ## Fin de process ##
                    
                    last_reco[i] = reco_file[0]
                self.nb_process += 1
    

###############################################################################
######                                                                   ######
######                             MAIN                                  ###### 
######                                                                   ######
###############################################################################

# ## Paramètres de la reconnaissance
keywords = ["Maman"]
n_mfcc = 30
delta = True
energy = False
n_subdivision = 1

## Création des threads
recorder1 = recorder(keywords, n_subdivision)
process1 = process(recorder1, keywords, n_subdivision)

## Lancement des threads
recorder1.start()
process1.start()

## Attente de finition 
recorder1.join()
process1.join()

###############################################################################
######                                                                   ######
######                             TEST                                  ###### 
######                                                                   ######
###############################################################################

# n_subdivision_max = 6
# time_reaction = []
# for n_subdivision in range(1, n_subdivision_max +1):
#     ## Création des threads
#     recorder1 = recorder(keyword, n_subdivision)
#     process1 = process(recorder1, keyword, n_subdivision)
    
#     ## Lancement des threads
#     recorder1.start()
#     process1.start()
    
#     ## Attente de finition 
#     recorder1.join()
#     process1.join()
    
#     ## Time reaction
#     time_reaction.append([a_i - b_i for a_i, b_i in zip(process1.time_buffer, recorder1.time_buffer)])

# import matplotlib.pyplot as plt
# import numpy as np
# plt.figure()
# n_subdivision = [n for n in range(1,len(time_reaction) -1)]
# n_subdivision_rasp = [n for n in range(1, 3)]
# time_reaction_avg_rasp = [0.626, 0.368]
# time_reaction_avg = [np.mean(time_reaction[i]) for i in range(len(time_reaction))]
# plt.plot(n_subdivision, time_reaction_avg[:4], label = 'Sur machine personnelle')
# plt.plot(n_subdivision_rasp, time_reaction_avg_rasp, label = 'Sur Raspberry Pi Model 3b+')
# plt.title('Temps de réaction en fonction du nombre de subdivision')
# plt.legend()
# plt.xlabel("Nombre de subdivision")
# plt.ylabel('Temps de réaction moyen (en s)')
