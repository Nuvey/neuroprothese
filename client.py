# Definition du client

import socket, sys, os, time, pyaudio, shutil, time
from recorder import Recorder

HOST = 'localhost'
PORT = 50000
CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
TRESHOLD = 8
TIME_OUT_RECORDING = 0.85
WAV_OUTPUT_FILENAME = "./Record"

# Efface le dossier d'enregistrements
try:
    shutil.rmtree(WAV_OUTPUT_FILENAME)
except:
    pass
os.makedirs(WAV_OUTPUT_FILENAME, exist_ok=True)


# Redefinition de la classe Recorder
class RecorderClient(Recorder):
    # Redefinition de la fonction record pour le client
    def record(self):
        print("C> Noise detected, Recording...")
        rec = []
        current_time = time.time()
        begin = current_time
        max_time = time.time() + TIME_OUT_RECORDING
        while current_time <= max_time:
            data = self.stream.read(CHUNK)
            if self.rms(data) >= TRESHOLD:
                max_time = time.time() + TIME_OUT_RECORDING
            current_time = time.time()
            rec.append(data)
        reactTime = current_time
        if (time.time() - begin < 2):
            self.write(b''.join(rec))
            msgClient = "Fichier ecrit"
            mySocket.send(msgClient.encode("Utf8"))
        else:
            msgClient = "Fichier non ecrit"
            mySocket.send(msgClient.encode("Utf8"))
        msgServeur = mySocket.recv(1024).decode("Utf8")
        print("S>", msgServeur)
        print("C> Temps de traitement : {}\n".format(time.time() - reactTime))
        print("C> Returning to listening")


# Creation de la socket
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Requete de connexion au serveur
requestTime = 3
while (1):
    i = 0
    try:
        mySocket.connect((HOST, PORT))
        break
    except socket.error:
        print("\rLa connexion a échoué.",end='')
        # sys.exit()
    while i < requestTime:
        print("\rTentative de reconnexion dans {}".format(requestTime-i),end='')
        i += 1
        time.sleep(1)
print("\nConnexion établie avec le serveur.")

# Discussion avec le serveur
msgServeur = mySocket.recv(1024).decode("Utf8")
# Changer etat de la LED (Raspberry uniquement)
try:
    os.system('sudo sh -c "echo none > /sys/class/leds/led0/trigger"')
    os.system('sudo sh -c "echo 1 > /sys/class/leds/led0/brightness"')
except:
    print("Pas de gestion de LED")
    pass

# Enregistrement continu
rec = RecorderClient()
rec.listen()

# Fermeture du client
print("Connexion interrompue.")
mySocket.close()
